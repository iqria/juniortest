<?php
return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'car',
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'rout',
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'driver',
    ]
];