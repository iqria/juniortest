<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Car".
 *
 * @property integer $carId
 * @property string $sign
 * @property string $mark
 * @property string $model
 *
 * @property Driver[] $drivers
 * @property Rout[] $routs
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carId', 'sign', 'mark', 'model'], 'required'],
            [['carId'], 'integer'],
            [['sign', 'mark', 'model'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'carId' => 'Car ID',
            'sign' => 'Sign',
            'mark' => 'Mark',
            'model' => 'Model',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['carId' => 'carId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouts()
    {
        return $this->hasMany(Rout::className(), ['carId' => 'carId']);
    }
}
