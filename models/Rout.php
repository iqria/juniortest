<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "Rout".
 *
 * @property integer $routId
 * @property string $date
 * @property integer $distance
 * @property integer $driverId
 * @property integer $carId
 *
 * @property Driver $driver
 * @property Car $car
 */
class Rout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Rout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['routId', 'date', 'distance', 'driverId', 'carId'], 'required'],
            [['routId', 'distance', 'driverId', 'carId'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'routId' => 'Rout ID',
            'date' => 'Date',
            'distance' => 'Distance',
            'driverId' => 'Driver ID',
            'carId' => 'Car ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['driverId' => 'driverId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['carId' => 'carId']);
    }

    public static function getTree()
    {
        $routs = static::find()->all();

        $routTree = [];
        foreach($routs as $rout) {
            $date = new DateTime($rout->date);
            $routTree[$date->format("Y")][$date->format("m")][$date->format("d")][$rout->carId][$rout->driverId] =  $rout->distance;
        }

        return $routTree;
    }
}
