<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Driver".
 *
 * @property integer $driverId
 * @property string $name
 * @property integer $carId
 *
 * @property Car $car
 * @property Rout[] $routs
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driverId', 'name', 'carId'], 'required'],
            [['driverId', 'carId'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'driverId' => 'Driver ID',
            'name' => 'Name',
            'carId' => 'Car ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['carId' => 'carId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouts()
    {
        return $this->hasMany(Rout::className(), ['driverId' => 'driverId']);
    }
}
