<?php

namespace app\controllers;

class DriverController extends \app\components\rest\ActiveController
{
    public $modelClass = "app\models\Driver";
}
