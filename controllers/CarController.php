<?php

namespace app\controllers;

class CarController extends \app\components\rest\ActiveController
{
    public $modelClass = "app\models\Car";
}
