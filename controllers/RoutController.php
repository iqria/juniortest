<?php

namespace app\controllers;

use app\models\Rout;
use Yii;

class RoutController extends \app\components\rest\ActiveController
{
    public $modelClass = "app\models\Rout";

    public function actions()
    {
        $actions = parent::actions();

        unset($actions["index"]);

        return $actions;
    }

    public function actionIndex()
    {
        return Rout::getTree();
    }
}
