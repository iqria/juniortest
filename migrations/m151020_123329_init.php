<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_123329_init extends Migration
{
    public function up()
    {
        /** Car Table */
        $this->execute("CREATE TABLE IF NOT EXISTS `Car` (
                          `carId` INT NOT NULL AUTO_INCREMENT,
                          `sign` VARCHAR(45) NOT NULL,
                          `mark` VARCHAR(45) NOT NULL,
                          `model` VARCHAR(45) NOT NULL,
                          PRIMARY KEY (`carId`))
                        ENGINE = InnoDB");

        /** Driver Table */
        $this->execute("CREATE TABLE IF NOT EXISTS `Driver` (
                      `driverId` INT NOT NULL AUTO_INCREMENT,
                      `name` VARCHAR(255) NOT NULL,
                      `carId` INT NULL,
                      PRIMARY KEY (`driverId`),
                      INDEX `fk_Driver_Car_idx` (`carId` ASC),
                      CONSTRAINT `fk_Driver_Car`
                        FOREIGN KEY (`carId`)
                        REFERENCES `Car` (`carId`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION)
                    ENGINE = InnoDB");

        /** Rout Table */
        $this->execute("CREATE TABLE IF NOT EXISTS `Rout` (
                      `routId` INT NOT NULL AUTO_INCREMENT,
                      `date` DATE NOT NULL,
                      `distance` INT NOT NULL,
                      `driverId` INT NOT NULL,
                      `carId` INT NOT NULL,
                      PRIMARY KEY (`routId`),
                      INDEX `fk_Rout_Driver1_idx` (`driverId` ASC),
                      INDEX `fk_Rout_Car1_idx` (`carId` ASC),
                      CONSTRAINT `fk_Rout_Driver1`
                        FOREIGN KEY (`driverId`)
                        REFERENCES `Driver` (`driverId`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                      CONSTRAINT `fk_Rout_Car1`
                        FOREIGN KEY (`carId`)
                        REFERENCES `Car` (`carId`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION)
                    ENGINE = InnoDB");
    }

    public function down()
    {
        echo "m151020_123329_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
